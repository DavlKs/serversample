package com.example;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.24.0)",
    comments = "Source: server.proto")
public final class CardServerGrpc {

  private CardServerGrpc() {}

  public static final String SERVICE_NAME = "com.example.CardServer";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.Server.GetCardNumberRequest,
      com.example.Server.GetCardNumberResponse> getGetCardNumberMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getCardNumber",
      requestType = com.example.Server.GetCardNumberRequest.class,
      responseType = com.example.Server.GetCardNumberResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.example.Server.GetCardNumberRequest,
      com.example.Server.GetCardNumberResponse> getGetCardNumberMethod() {
    io.grpc.MethodDescriptor<com.example.Server.GetCardNumberRequest, com.example.Server.GetCardNumberResponse> getGetCardNumberMethod;
    if ((getGetCardNumberMethod = CardServerGrpc.getGetCardNumberMethod) == null) {
      synchronized (CardServerGrpc.class) {
        if ((getGetCardNumberMethod = CardServerGrpc.getGetCardNumberMethod) == null) {
          CardServerGrpc.getGetCardNumberMethod = getGetCardNumberMethod =
              io.grpc.MethodDescriptor.<com.example.Server.GetCardNumberRequest, com.example.Server.GetCardNumberResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getCardNumber"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.Server.GetCardNumberRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.Server.GetCardNumberResponse.getDefaultInstance()))
              .setSchemaDescriptor(new CardServerMethodDescriptorSupplier("getCardNumber"))
              .build();
        }
      }
    }
    return getGetCardNumberMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CardServerStub newStub(io.grpc.Channel channel) {
    return new CardServerStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CardServerBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CardServerBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CardServerFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CardServerFutureStub(channel);
  }

  /**
   */
  public static abstract class CardServerImplBase implements io.grpc.BindableService {

    /**
     */
    public void getCardNumber(com.example.Server.GetCardNumberRequest request,
        io.grpc.stub.StreamObserver<com.example.Server.GetCardNumberResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetCardNumberMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetCardNumberMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.example.Server.GetCardNumberRequest,
                com.example.Server.GetCardNumberResponse>(
                  this, METHODID_GET_CARD_NUMBER)))
          .build();
    }
  }

  /**
   */
  public static final class CardServerStub extends io.grpc.stub.AbstractStub<CardServerStub> {
    private CardServerStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CardServerStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CardServerStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CardServerStub(channel, callOptions);
    }

    /**
     */
    public void getCardNumber(com.example.Server.GetCardNumberRequest request,
        io.grpc.stub.StreamObserver<com.example.Server.GetCardNumberResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetCardNumberMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CardServerBlockingStub extends io.grpc.stub.AbstractStub<CardServerBlockingStub> {
    private CardServerBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CardServerBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CardServerBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CardServerBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.example.Server.GetCardNumberResponse getCardNumber(com.example.Server.GetCardNumberRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetCardNumberMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CardServerFutureStub extends io.grpc.stub.AbstractStub<CardServerFutureStub> {
    private CardServerFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CardServerFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CardServerFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CardServerFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.example.Server.GetCardNumberResponse> getCardNumber(
        com.example.Server.GetCardNumberRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetCardNumberMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_CARD_NUMBER = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CardServerImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CardServerImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_CARD_NUMBER:
          serviceImpl.getCardNumber((com.example.Server.GetCardNumberRequest) request,
              (io.grpc.stub.StreamObserver<com.example.Server.GetCardNumberResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CardServerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CardServerBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.Server.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CardServer");
    }
  }

  private static final class CardServerFileDescriptorSupplier
      extends CardServerBaseDescriptorSupplier {
    CardServerFileDescriptorSupplier() {}
  }

  private static final class CardServerMethodDescriptorSupplier
      extends CardServerBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CardServerMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CardServerGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CardServerFileDescriptorSupplier())
              .addMethod(getGetCardNumberMethod())
              .build();
        }
      }
    }
    return result;
  }
}
