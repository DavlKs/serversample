package org.example;


import com.example.CardServerGrpc;
import com.example.Server;
import io.grpc.stub.StreamObserver;

import java.util.HashMap;
import java.util.Map;

public class SimpleServer extends CardServerGrpc.CardServerImplBase {


    Map<Integer, String> mapOfCardNumbers = fillMapWithCardNumbers();


    public void getCardNumber(Server.GetCardNumberRequest request,
                              StreamObserver<Server.GetCardNumberResponse> responseObserver) {

        Server.GetCardNumberResponse getCardNumberResponse = Server.GetCardNumberResponse
                .newBuilder()
                .setCardNumber(mapOfCardNumbers.get(request.getCardId()))
                .build();

        responseObserver.onNext(getCardNumberResponse);

        responseObserver.onCompleted();

    }

    public Map<Integer, String> fillMapWithCardNumbers() {
        Map<Integer, String> mapOfCardNumbers = new HashMap<>();
        mapOfCardNumbers.put(1, "4093027584027483");
        mapOfCardNumbers.put(2, "4083750285730274");
        return mapOfCardNumbers;
    }
}
