package org.example;


import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args) throws IOException, InterruptedException {

        Server server = ServerBuilder
                .forPort(8085)
                .addService(new SimpleServer())
                .build();

        server.start();

        server.awaitTermination();

    }
}
